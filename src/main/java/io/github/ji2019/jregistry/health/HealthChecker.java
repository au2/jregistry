package io.github.ji2019.jregistry.health;

/**
 * Interface for health checker.
 *
 * 
 * @create 2024/4/13 20:41
 */
public interface HealthChecker {

    void start();
    void stop();

}
