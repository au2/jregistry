package io.github.ji2019.jregistry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({JRegistryConfigProperties.class})
public class JRegistryApplication {

    public static void main(String[] args) {
        SpringApplication.run(JRegistryApplication.class, args);
    }

}
