package io.github.ji2019.jregistry;

import io.github.ji2019.jregistry.cluster.Cluster;
import io.github.ji2019.jregistry.health.HealthChecker;
import io.github.ji2019.jregistry.health.JHealthChecker;
import io.github.ji2019.jregistry.health.JHealthChecker;
import io.github.ji2019.jregistry.service.JRegistryService;
import io.github.ji2019.jregistry.service.RegistryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * configuration for all beans.
 *
 * 
 * @create 2024/4/13 19:50
 */

@Configuration
public class JRegistryConfig {

    @Bean
    public RegistryService registryService()
    {
        return new JRegistryService();
    }

    @Bean(initMethod = "start", destroyMethod = "stop")
    public HealthChecker healthChecker(@Autowired RegistryService registryService) {
        return new JHealthChecker(registryService);
    }

    @Bean(initMethod = "init")
    public Cluster cluster(@Autowired JRegistryConfigProperties registryConfigProperties) {
        return new Cluster(registryConfigProperties);
    }

}
