package io.github.ji2019.jregistry;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * registry config properties.
 *
 * 
 * @create 2024/4/16 20:24
 */

@Data
@ConfigurationProperties(prefix = "jregistry")
public class JRegistryConfigProperties {

    private List<String> serverList;

}
